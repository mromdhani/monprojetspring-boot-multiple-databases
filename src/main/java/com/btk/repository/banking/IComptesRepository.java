package com.btk.repository.banking;

import org.springframework.data.jpa.repository.JpaRepository;

import com.btk.domain.banking.Compte;

public interface IComptesRepository extends JpaRepository<Compte, String> {	

}
