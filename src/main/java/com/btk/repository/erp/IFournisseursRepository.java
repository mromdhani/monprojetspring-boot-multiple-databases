package com.btk.repository.erp;

import org.springframework.data.jpa.repository.JpaRepository;

import com.btk.domain.erp.Fournisseur;

public interface IFournisseursRepository extends JpaRepository<Fournisseur, String> {

}
