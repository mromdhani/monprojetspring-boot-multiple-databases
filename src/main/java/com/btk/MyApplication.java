package com.btk;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.btk.domain.banking.Compte;
import com.btk.domain.erp.Fournisseur;
import com.btk.repository.banking.IComptesRepository;
import com.btk.repository.erp.IFournisseursRepository;

@SpringBootApplication
public class MyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyApplication.class, args);
	}

  @Autowired
  IComptesRepository repoComptes;
  
  @Autowired
  IFournisseursRepository repoFournisseurs;
  @Bean
  CommandLineRunner runIt() {
	  return args -> {
		  repoComptes.save(new Compte("BTK001", "Rod Johnson Spring", new BigDecimal("1000")));
	      repoFournisseurs.save(new Fournisseur("FOUR 001", "Dave Seyer", "sayer@spring.org", "Wahington USA"));
	  };
  }

}

