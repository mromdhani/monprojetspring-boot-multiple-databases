package com.btk.config;

import static java.util.Collections.singletonMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		entityManagerFactoryRef = "erpEntityManager", 
		transactionManagerRef = "erpTransactionManager", 
		basePackages = "com.btk.repository.erp"
)
public class ErpDBConfig {

	@Bean
	@ConfigurationProperties(prefix = "spring.erp.datasource")
	public DataSource mysqlErpDataSource() {
		return DataSourceBuilder
					.create()
					.build();
	}

	@Bean(name = "erpEntityManager")
	public LocalContainerEntityManagerFactoryBean mysqlErpEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder
					.dataSource(mysqlErpDataSource())
					.packages("com.btk.domain.erp")
					.persistenceUnit("erpPU")
					.properties(singletonMap("hibernate.hbm2ddl.auto", "create-drop"))
					.build();
	}

	@Bean(name = "erpTransactionManager")
	public PlatformTransactionManager mysqlErpTransactionManager(@Qualifier("erpEntityManager") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

	
}