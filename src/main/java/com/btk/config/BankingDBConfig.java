package com.btk.config;

import java.util.HashMap;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import static java.util.Collections.singletonMap;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(
		entityManagerFactoryRef = "bankingEntityManager", 
		transactionManagerRef = "bankingTransactionManager", 
		basePackages = "com.btk.repository.banking"
)
public class BankingDBConfig {

	@Primary
	@Bean
	@ConfigurationProperties(prefix = "spring.banking.datasource")
	public DataSource mysqlBankigDataSource() {
		return DataSourceBuilder
					.create()
					.build();
	}

	@Primary
	@Bean(name = "bankingEntityManager")
	public LocalContainerEntityManagerFactoryBean mysqlBankingEntityManagerFactory(EntityManagerFactoryBuilder builder) {
		return builder
					.dataSource(mysqlBankigDataSource())
					.packages("com.btk.domain.banking")
					.persistenceUnit("bookingPU")
					.properties(singletonMap("hibernate.hbm2ddl.auto", "create-drop"))
					.build();
	}

	@Primary
	@Bean(name = "bankingTransactionManager")
	public PlatformTransactionManager mysqlBankingTransactionManager(@Qualifier("bankingEntityManager") EntityManagerFactory entityManagerFactory) {
		return new JpaTransactionManager(entityManagerFactory);
	}

	
}