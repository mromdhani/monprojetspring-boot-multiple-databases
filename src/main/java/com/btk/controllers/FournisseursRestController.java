package com.btk.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.btk.domain.erp.Fournisseur;
import com.btk.repository.erp.IFournisseursRepository;

@RestController
@RequestMapping("/fournisseurs")
public class FournisseursRestController {

	private IFournisseursRepository repository;
	
	public FournisseursRestController(IFournisseursRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping
	public List<Fournisseur> tousLesFournisseurs() {
		return repository.findAll();
	}
}
