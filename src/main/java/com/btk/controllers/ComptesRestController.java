package com.btk.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.btk.domain.banking.Compte;
import com.btk.repository.banking.IComptesRepository;

@RestController
@RequestMapping("/comptes")
public class ComptesRestController {

	private IComptesRepository repository;
	
	public ComptesRestController(IComptesRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping
	public List<Compte> tousLesComptes() {
		return repository.findAll();
	}
}
